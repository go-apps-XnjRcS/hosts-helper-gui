package main

import (
	"app/models"
	"github.com/fatih/color"
	"github.com/leaanthony/mewn"
	"github.com/wailsapp/wails"
	"log"
	"os/exec"
	"strings"
)

func HostsInit() *models.HostsStruct {
	//HS := &models.HostsStruct{Path: "/home/sht/projects/go-desktop-apps/host-helper-gui/src/app/fake.txt"}
	HS := &models.HostsStruct{Path: "/etc/hosts"}
	return HS
}

func IsSudo() bool {
	//подробнее https://www.socketloop.com/tutorials/golang-force-your-program-to-run-with-root-permissions
	//а также http://qaru.site/questions/617200/convert-byte-array-to-int-using-go-language/2458440#2458440

	idByte, err := exec.Command("id", "-u").Output()
	id := strings.TrimSpace(string(idByte))

	if err != nil {
		log.Fatal(err)
	}

	return id == "0"
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	if !IsSudo() {
		color.Red("Please run me with %q", "sudo")
		return
	}

	js := mewn.String("./frontend/dist/app.js")
	css := mewn.String("./frontend/dist/app.css")

	app := wails.CreateApp(&wails.AppConfig{
		Width:     460,
		Height:    768,
		Title:     "SHT-HOSTS-HELPER",
		JS:        js,
		CSS:       css,
		Colour:    "#fff",
		Resizable: true,
	})

	app.Bind(HostsInit())
	err := app.Run()
	if err != nil {
		log.Fatal(err)
	}
}
