import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.config.devtools = true;

import Bridge from "./wailsbridge";
import './quasar'
import router from './router'
import store from './store'
import _ from 'lodash'

window._ = _

Bridge.Start(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount("#app");
});
