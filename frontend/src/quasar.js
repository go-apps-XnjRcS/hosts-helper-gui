import Vue from 'vue'

import './styles/quasar.styl'
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'

import {
  ClosePopup,
  Quasar,
  QAvatar,
  QCard,
  QCardSection,
  QCardActions,
  QFooter,
  QForm,
  QLayout,
  QHeader,
  QDrawer,
  QDialog,
  Dialog,
  QPopupEdit,
  QPageContainer,
  QPage,
  QMarkupTable,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QIcon,
  QList,
  QItem,
  Notify,
  QInput,
  QTabs,
  QTable,
  QTh,
  QTr,
  QTd,
  QRouteTab,
  QSpace,
  QTab,
  QItemSection,
  QItemLabel,
} from 'quasar'

Vue.use(Quasar, {
  components: {
    QLayout,
    QAvatar,
    QCard,
    QCardSection,
    QCardActions,
    QFooter,
    QForm,
    QHeader,
    QDialog,
    QDrawer,
    QPopupEdit,
    QPageContainer,
    QPage,
    QMarkupTable,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QInput,
    QList,
    QItem,
    Notify,
    QItemSection,
    QItemLabel,
    QTabs,
    QTable,
    QTh,
    QTr,
    QTd,
    QRouteTab,
    QSpace,
    QTab,
  },
  directives: {
    ClosePopup
  },
  plugins: {
    Notify, Dialog
  },
  config: {
    notify: { /* Notify defaults */ }
  }
})