module app

require (
	github.com/fatih/color v1.7.0
	github.com/leaanthony/mewn v0.10.7
	github.com/pkg/errors v0.8.1
	github.com/wailsapp/wails v0.17.0
)

go 1.13
