### SHT-HOSTS-HELPER. GUI version

That is a simple tool for management **hosts** file. This is useful for example for developers who need to work with local hosts. And often have to edit /etc/hosts. To simplify this task, I decided to write this simple program. First of all, for personal needs, as well as for educational purposes.

This program, as well as the code, you can use under a MIT license.

#### How to use
It's a very simple, just look at screencast below

![](./assets/sht-hosts-helper-gui.mp4)

#### How to install

- Download [binary file](./bin/sht-hosts-helper-gui).
- `$ sudo chmod +x ./sht-hosts-helper-gui`
- `$ mv ./sht-hosts-helper-gui /usr/local/bin`
- `$ sudo sht-hosts-helper-gui`

#### I used:

- [Wails.app](https://wails.app) for bind GO-backend with VUE-frontend
- [Quasar](https://quasar.dev) as frontend framework

