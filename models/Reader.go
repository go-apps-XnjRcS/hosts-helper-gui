package models

import (
	"bufio"
	"github.com/pkg/errors"
	"os"
	"strings"
)

func (O *HostsStruct) Init() error {
	O.hosts = make(map[string][]string)
	var linesArr []string

	if O.Path == "" {
		return errors.New("File path not specified")
	}

	file, err := os.Open(O.Path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lineStr := scanner.Text()
		linesArr = append(linesArr, lineStr)
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return O.parseLines(linesArr)
}

//Parse lines from /etc/hosts
func (O *HostsStruct) parseLines(lines []string) error {
	if len(lines) == 0 {
		return nil
	}

	for _, line := range lines {
		fields := strings.Fields(line)
		if len(fields) < 2 {
			continue
		}
		/*if fields[0] != "127.0.0.1" {
			Others[fields[0]] = append(Others[fields[0]], fields[1:]...)
			continue
		}*/

		O.hosts[fields[0]] = append(O.hosts[fields[0]], fields[1:]...)
	}

	return nil
}

func (O *HostsStruct) ShowHosts() map[string][]string {
	return O.hosts
}
