package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

type hostsJson struct {
	Hosts map[string][]string `json:"hosts"`
}

func (O *HostsStruct) SaveToFile(hostsStr string) (bool, error) {
	var data []byte
	var jsonRes hostsJson

	if err := json.Unmarshal([]byte(hostsStr), &jsonRes); err != nil {
		return false, err
	}

	for ip, hosts := range jsonRes.Hosts {
		s := fmt.Sprintf("%s %s\n", ip, strings.Join(hosts, " "))
		data = append(data, s...)
	}

	err := ioutil.WriteFile(O.Path, data, 0644)
	if err != nil {
		return false, err
	}

	O.hosts = jsonRes.Hosts

	return true, nil
}
