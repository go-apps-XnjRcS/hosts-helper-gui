package models

import (
	"fmt"
	"testing"
)

func TestHostsStruct_SaveToFile(t *testing.T) {
	HS := &HostsStruct{Path: "/home/sht/projects/go-desktop-apps/host-helper-gui/src/app/fake.txt"}
	err := HS.Init()
	if err != nil {
		t.Error(err)
	}

	fakeJson := `{"hosts": {"32323232":["sdsadasdasd"],"127.0.0.1":["dsfdsdsf","fdgdfgfdgdfg","jhgjhgjhgjghj","23432wwerwer"]}}`

	res, err := HS.SaveToFile(fakeJson)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(res)

}
